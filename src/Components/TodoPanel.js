import React, { Component } from 'react';
import './TodoPanel.css';

class TodoPanel extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputValue: '',
            todos: []
        };
        this.inputChange = this.inputChange.bind(this);
    }

    componentDidMount(){
        const axios = require('axios');
        axios.get('https://api.mlab.com/api/1/databases/tododb/collections/todos?apiKey=4XG5ZZpXih69hKeSsNtjaTqqyTbPw0En')
        .then(response => {
            this.setState({
                todos: response.data
            });
        }); 
    }

    inputChange = (event) => {
        this.setState({
            inputValue: event.target.value
        });       
    }

    removeTodo = (id) => {
        const newTodos = this.state.todos.filter(todo => todo._id.$oid != id);
        this.setState({
            todos: newTodos
        }); 

        const axios = require('axios');
        axios.delete('https://api.mlab.com/api/1/databases/tododb/collections/todos/' + id + '?apiKey=4XG5ZZpXih69hKeSsNtjaTqqyTbPw0En')
        .then(response => {
                    
        });
    }
  
    addTodo = () => {
        if (this.state.inputValue != '') {
        const axios = require('axios');
        axios.post('https://api.mlab.com/api/1/databases/tododb/collections/todos?apiKey=4XG5ZZpXih69hKeSsNtjaTqqyTbPw0En',
        { 
            name: this.state.inputValue,
            completed: false
        })
        .then(response => {
            this.setState(prevState => {
                return prevState.todos.push(response.data);
            });
        });
        this.setState({
            inputValue: ''
        });
        }
    }

    completeTodo = (id) => {
        this.setState(prevState => {
            const prevTodos = prevState.todos;
            for (let i = 0; i < prevTodos.length; i++) {
                if (prevTodos[i]._id.$oid == id) {
                    prevTodos[i].completed = !prevTodos[i].completed;

                    const axios = require('axios');
                    axios.put('https://api.mlab.com/api/1/databases/tododb/collections/todos/' + id + '?apiKey=4XG5ZZpXih69hKeSsNtjaTqqyTbPw0En',
                    prevTodos[i])
                    .then(response => {
            
                    });
                } 
            }
            return {
                todos: prevTodos
            }           
        });       
    }

    render() {
    return (
      <div className="TodoPanel">
        <h1>To-do list:</h1>
        <ul>
            {this.state.todos.map(todo => 
                <li key={todo._id.$oid} onClick={this.completeTodo.bind(this, todo._id.$oid)} style={todo.completed ? {textDecoration: 'line-through'} : {textDecoration: 'none'}}>
                <span style={todo.completed ? {} : {display: 'none'}}><i className="far fa-check-circle"></i></span>
                <span style={todo.completed ? {display: 'none'} : {}}><i className="far fa-circle"></i></span>                
                {todo.name}
                <button type='button' onClick={this.removeTodo.bind(this, todo._id.$oid)}><i className="far fa-trash-alt"></i></button>
                </li>
            )}
            <li className='AddListItem'>
                <input type='text' value={this.state.inputValue} onChange={this.inputChange} placeholder='Add new To-do'/>
                <button type='button' onClick={this.addTodo}><i className="fas fa-plus"></i></button>
            </li>
        </ul>
      </div>
    );
  }
}

export default TodoPanel;
