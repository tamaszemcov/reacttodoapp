import React, { Component } from 'react';
import './App.css';
import TodoPanel from './Components/TodoPanel'

class App extends Component {
  render() {
    return (
      <div className="App">
        <TodoPanel />
      </div>
    );
  }
}

export default App;
